/**
 * 
 */
package com.invest.cardpay.proxy;

import java.io.IOException;
import java.io.StringReader;
import java.util.Base64;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * @author Tabakov
 *
 */
@Path("/api")
public class Proxy {

	private static final Logger logger = LogManager.getLogger(Proxy.class);
	public static final String XML_DESCRIPTION_ATTRIBUTE = "description";
	public static final String XML_NOTE_ATTRIBUTE = "note";
	private static final String XML_ORDER_ELEMENT = "order";

	@POST
	@Path("/proxy")
	@Produces(MediaType.APPLICATION_JSON)
	public String cardpay(@FormParam("orderXML") String orderXML, @FormParam("sha512") String sha512) {

		logger.debug("orderXML encoded from cardpay: " + orderXML);
		logger.debug("sha512 encoded from cardpay: " + sha512);

		try {
			CardPayInfo info = constructResult(orderXML, sha512);
			logger.debug("Start redirecting to: " + info.getNote());
			String res = postForm(info);
			logger.debug("Response from post request is: " + res);
			return res;

		} catch (Exception e) {
			logger.debug("CardPay proxy can not perform the redirection due to: ", e);
			return null;
		}
	}

	private String decodeOrderXml(String orderXML) throws Exception {
		byte[] asBytes = Base64.getDecoder().decode(orderXML);
		return new String(asBytes, "utf-8");
	}

	private String postForm(CardPayInfo info) {
		logger.debug("Create post request to: " + info.getNote() + "with params: orderXML=" + info.getOrderXMLEncoded() + "and sha512=" + info.getSha512());
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(info.getNote());
		Form form = new Form();
		form.param("orderXML", info.getOrderXMLEncoded()).param("sha512", info.getSha512());
		Entity<Form> entity = Entity.form(form);
		Response response = target.request(MediaType.APPLICATION_FORM_URLENCODED).post(entity);
		String value = response.readEntity(String.class);
		return value;
	}

	private CardPayInfo constructResult(String orderXMLEncoded, String sha512) throws Exception {
		CardPayInfo info = new CardPayInfo(orderXMLEncoded);
		info.setOrderXML(decodeOrderXml(info.getOrderXMLEncoded()));
		info.setSha512(sha512);
		logger.info("orderXML decoded: " + info.getOrderXML());

		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = builder.parse(new InputSource(new StringReader(info.getOrderXML())));
			NodeList elements = document.getElementsByTagName(XML_ORDER_ELEMENT);
			if (elements.getLength() > 0) {
				Node order = elements.item(0);
				NamedNodeMap attributes = order.getAttributes();
				info.setNote(attributes.getNamedItem(XML_NOTE_ATTRIBUTE).getNodeValue());
			}
			return info;
		} catch (ParserConfigurationException | SAXException | IOException e) {
			logger.debug("Unable to parse xml parameters: ", e);
			throw e;
		}
	}

}
