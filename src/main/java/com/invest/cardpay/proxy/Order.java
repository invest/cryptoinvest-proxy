/**
 * 
 */
package com.invest.cardpay.proxy;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tabakov
 *
 */
@XmlRootElement(name = "order")
public class Order {

	private int wallet_id;
	private int number;
	private String description;
	private String currency;
	private double amount;
	private String email;

	public int getWallet_id() {
		return wallet_id;
	}

	@XmlAttribute
	public void setWallet_id(int wallet_id) {
		this.wallet_id = wallet_id;
	}

	public int getNumber() {
		return number;
	}

	@XmlAttribute
	public void setNumber(int number) {
		this.number = number;
	}

	public String getDescription() {
		return description;
	}

	@XmlAttribute
	public void setDescription(String description) {
		this.description = description;
	}

	public String getCurrency() {
		return currency;
	}

	@XmlAttribute
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getAmount() {
		return amount;
	}

	@XmlAttribute
	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getEmail() {
		return email;
	}

	@XmlAttribute
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Order [wallet_id=" + wallet_id + ", number=" + number + ", description=" + description + ", currency="
				+ currency + ", amount=" + amount + ", email=" + email + "]";
	}

}
